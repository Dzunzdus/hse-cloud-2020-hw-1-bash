#!/bin/bash
read N
res=0
for i in $(seq 1 $N)
do
  read tmp
  (( res += tmp ))
done
awk "BEGIN {printf \"%.3f\n\", $res / $N}"