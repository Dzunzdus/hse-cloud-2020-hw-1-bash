#!/bin/bash
read x
read y
read z
if [[ $x == $y && $y == $z ]]; then
  echo -n "EQUILATERAL"
elif [[ $x == $y || $y == $z ]]; then
  echo -n "ISOSCELES"
else
  echo -n "SCALENE"
fi