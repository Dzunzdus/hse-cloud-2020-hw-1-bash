#!/bin/bash
readarray arr
for i in $(seq 1 ${#arr[@]})
do
  idx=$(($i-1))
  if [[ "${arr[idx]}" != *"a"* || "${arr[idx]}" == *"A"* ]]; then
    echo ${arr[idx]}
  fi
done