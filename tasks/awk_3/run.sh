#!/bin/bash
awk '{
  aver=($2+$3+$4)/3; 
  if (aver<50) {
    print $0 " : FAIL"
  } else if (aver<60) {
    print $0 " : C"
  } else if (aver < 80) {
    print $0 " : B"
  } else {
    print $0 " : A"
  }
}'