#!/bin/bash
readarray arr
for i in $(seq 1 ${#arr[@]})
do
  idx=$(($i-1))
  if [ ${#arr[idx]} -gt 0 ]; then
    arr[$idx]=".${arr[idx]:1}"
  fi
done
echo ${arr[@]}