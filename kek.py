import os
import subprocess

dirs = os.listdir('./tasks')

for curdir in dirs:
  curfile = "tasks/{}/run.sh".format(curdir)
  os.system("sed -i '1s/^/\#!\/bin\/bash\n/' {}".format(curfile))